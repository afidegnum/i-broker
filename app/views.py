from flask import render_template
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelView
from app import appbuilder, db
from .models import Content


class ContentView(ModelView):
    datamodel = SQLAInterface(Content)
    list_columns = ['id', 'name', 'intro', 'body']
    add_columns = ['name', 'intro', 'body']
    label_columns = {'name': 'Name', 'intro': 'Introduction'}


appbuilder.add_view(ContentView, "My View", icon="fa-folder-open-o", category="My Category",
                    category_icon='fa-envelope')


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 404


db.create_all()
