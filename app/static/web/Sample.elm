module Main exposing (..)

import Html exposing (Html, text)
import Json.Decode as JD exposing (Decoder)


main : Html a
main =
    text <| toString <| JD.decodeString decodeTable sampleJson


sampleJson : String
sampleJson =
    """
{

  "count": 2,

  "label_columns": {

    "body": "Body",

    "id": "Id",

    "intro": "Intro",

    "name": "Name"

  },

  "list_columns": [

    "id",

    "name",

    "intro",

    "body"

  ],

  "modelview_name": "ContentView",

  "order_columns": [

    "id",

    "name",

    "intro",

    "body"

  ],

  "page": null,

  "page_size": null,

  "pks": [

    1,

    2

  ],

  "result": [

    {

      "body": "welcome to my list view\\r\\nwe need to find out more",

      "id": 1,

      "intro": "my intro",

      "name": "this name"

    },

    {

      "body": "this is the body page with it's crude form kudo to the original owner, ",

      "id": 2,

      "intro": "a new way to get in touch with more app ",

      "name": "this name"

    }

  ]

}
    """


type alias Table =
    List Column


type Column
    = IntColumn String String (List Int)
    | StringColumn String String (List String)


decodeTable : Decoder Table
decodeTable =
    decodeTableMetaData |> JD.andThen decodeColumns


decodeTableMetaData : Decoder Table
decodeTableMetaData =
    JD.field "label_columns"
        (JD.keyValuePairs JD.string
            |> JD.map (List.map (uncurry mkColumn))
        )


mkColumn : String -> String -> Column
mkColumn name label =
    if name == "id" then
        IntColumn name label []
    else
        StringColumn name label []


decodeColumns : Table -> Decoder Table
decodeColumns table =
    table
        |> List.map decodeForColumn
        |> combine


decodeForColumn : Column -> Decoder Column
decodeForColumn column =
    JD.field "result"
        (case column of
            IntColumn name label _ ->
                typedColumn (IntColumn name label) name JD.int

            StringColumn name label _ ->
                typedColumn (StringColumn name label) name JD.string
        )


typedColumn : (List a -> column) -> String -> Decoder a -> Decoder column
typedColumn columnF field decoder =
    JD.list (JD.field field decoder)
        |> JD.map columnF


combine : List (Decoder a) -> Decoder (List a)
combine =
    List.foldr (JD.map2 (::)) (JD.succeed [])
