module Update exposing (update)

import Http exposing (..)
import Json.Decode exposing (..)
import Types exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    -- case msg of
    --     Username str ->
    --         ( { model | loginUserName = str }, Cmd.none )
    --
    --     Password str ->
    --         ( { model | loginPassword = str }, Cmd.none )
    --
    --     Login ->
    --         ( model, login model.loginUserName model.loginPassword )
    --
    --     LoginResult (Ok username) ->
    --         ( { model | user = LoggedInUser { userName = username } }
    --         , Cmd.none
    --         )
    --
    --     LoginResult (Err _) ->
    --         ( model, Cmd.none )
    --
    --     Logout ->
    --         ( model, Cmd.none )




myurl : String -> Cmd Msg
myurl : url =
    let
        viewlink = url
        request = Http.get url decodeTable
    in
        Http.send LoginResult request





-- Or
--decodeLogin : Decoder String
--decodeLogin = decodeString (field "username" string)
