import datetime

from flask import url_for
from flask_appbuilder import Model
from flask_appbuilder.filemanager import ImageManager
from flask_appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn
from markupsafe import Markup
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Date, func, Table
from sqlalchemy.orm import relationship
from jinja2 import Template
from flask_appbuilder.security.sqla.models import User


def today():
    return datetime.datetime.today()


class Content(Model):
    __tablename__ = 'content'
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    intro = Column(String(350), unique=False, nullable=True)
    body = Column(Text)

    def __repr__(self):
        return self.name


class Policy(Model):
    __tablename__ = 'policy'
    id = Column(Integer, primary_key=True)
    name = Column(String(100), unique=True, nullable=False)
    description = Column(Text)
    valid_from = Column(Date, default=func.now())
    valid_to = Column(Date, default=func.now())

    def __repr__(self):
        return self.name


class Quote(Model):
    __tablename__ = 'quote'
    id = Column(Integer, primary_key=True)
    name = Column(String(100), unique=True, nullable=False)
    datetime = Column(Date, default=func.now())
    valid_from = Column(Date, default=func.now())  # inport sqlalchemy's func
    valid_to = Column(Date, default=func.now())  # inport sqlalchemy's func
    description = Column(Text)
    user_id = Column(Integer, ForeignKey('ab_user.id'))
    policy_id = Column(Integer, ForeignKey('policy.id'))

    def __repr__(self):
        return self.name


class Inscategory(Model):
    __tablename__ = 'inscategory'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    type = Column(String(50))

    __mapper_args__ = {
        'polymorphic_identity': 'inscategory',
        'polymorphic_on': type
    }

    def __repr__(self):
        return "Inscategory %s" % self.name


class Property(Inscategory):
    __tablename__ = 'property'
    id = Column(Integer, ForeignKey('inscategory.id'), primary_key=True)
    engineer_name = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'property',
    }


class Casuality(Inscategory):
    __tablename__ = 'casuality'
    id = Column(Integer, ForeignKey('inscategory.id'), primary_key=True)
    engineer_name = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'casuality',
    }


class Life(Inscategory):
    __tablename__ = 'life'
    id = Column(Integer, ForeignKey('inscategory.id'), primary_key=True)
    engineer_name = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'life',
    }


class Disability(Inscategory):
    __tablename__ = 'disability'
    id = Column(Integer, ForeignKey('inscategory.id'), primary_key=True)
    engineer_name = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'disability',
    }


class Health(Inscategory):
    __tablename__ = 'health'
    id = Column(Integer, ForeignKey('inscategory.id'), primary_key=True)
    engineer_name = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'health',
    }


#
# user_policys = Table('user_policys',
#     Column('user_id', Integer, ForeignKey('user.id')),
#     Column('policy_id', Integer, ForeignKey('policy.id'))
# )  # DB RELATIONSHIP to any of them
# TODO REST API Linking to Ins. Corps.
# commission tracking... count, ... value etc... refers to payment grid convension for ins. corpos.

class Paymentschedule(Model):
    __tablename__ = 'paymentschedule'
    id = Column(Integer, primary_key=True)
    name = Column(String(100), unique=True, nullable=False)
    user_id = Column(Integer, ForeignKey('ab_user.id'))
    payment_date = Column(Date, default=func.now())  # inport sqlalchemy's func
    amount = Column(Integer)

    # TODO tie to custom payment invoice

    def __repr__(self):
        return self.name

# save current user's country in his country extra model for performance.


# # Organisation structure
#
# class Quote(Model):
#     id = Column(Integer, primary_key=True)
#     ins_prd_id = Column(Integer, ForeignKey('ins_prd.id'))
#     body = Column(Text, unique=True, nullable=False)
#     valid_to = Column(DateTime(timezone=True), default=func.now())
#
#     def __repr__(self):
#         return self.name

#
# class OrgType(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
# class Relation(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
# class UserDetail(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
#
#
# class Address(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
# # Events
#
#
#
# class Event(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
#
# class EventType(Model):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)
#     body = Column(Text, unique=True, nullable=False)
#
#     def __repr__(self):
#         return self.name
#
#
# #
# # class InsurancePolicy(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# #
# # class Policies(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# #
# # class Insured(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# #
# # class Contract(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# # class Transaction(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# # class TransactionType(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# #
# #
# #
# # class Life(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# # class Motor(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# # class Property(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# # class Claim(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# # class ClaimStatus(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# # class ClaimOutcome(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
# #
# # class PaymentSchedule(Model):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(50), unique=True, nullable=False)
# #     body = Column(Text, unique=True, nullable=False)
# #
# #     def __repr__(self):
# #         return self.name
# #
# #
